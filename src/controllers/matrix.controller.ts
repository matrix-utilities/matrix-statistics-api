import { Request, Response } from 'express';
import { MatrixService } from '../services/matrix.service';
import { Matrix } from '../interfaces/matrix.interface';

/**
 * @openapi
 * /stats/api/v1/calculate:
 *   post:
 *     summary: Calculate matrix statistics
 *     description: Receives a matrix and calculates various statistics.
 *     tags:
 *       - Matrix
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Matrix'
 *           example:
 *             data:
 *               - [1, 3, 5]
 *               - [1, 3, 1]
 *               - [2, -1, 7]
 *     responses:
 *       200:
 *         description: Successful calculation of matrix statistics
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MatrixStatistics'
 *             example:
 *               code: '01'
 *               data:
 *                 max: 7
 *                 min: -1
 *                 average: 2.4444444444444446
 *                 totalSum: 22
 *                 isDiagonal: false
 *       400:
 *         description: Error in calculation
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Error'
 *             example:
 *               code: '99'
 *               data: 'Cannot read properties of undefined.'
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Error'
 *             example:
 *               code: '99'
 *               data: 'Unauthorized error.'
 *
 * @openapi
 * components:
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 *   schemas:
 *     Matrix:
 *       type: object
 *       properties:
 *         data:
 *           type: array
 *           items:
 *             type: array
 *             items:
 *               type: number
 *     MatrixStatistics:
 *       type: object
 *       properties:
 *         max:
 *           type: number
 *         min:
 *           type: number
 *         average:
 *           type: number
 *         totalSum:
 *           type: number
 *         isDiagonal:
 *           type: boolean
 *     Error:
 *       type: object
 *       properties:
 *         error:
 *           type: string
 *         code:
 *           type: string
 *         data:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 * security:
 *   - bearerAuth: []
 */
export const postMatrix = (req: Request, res: Response) => {
    const matrix: Matrix = req.body;

    try {
        const stats = MatrixService.calculateStatistics(matrix);
        res.json({ code: '01', data: stats });
    } catch (error: any) {
        res.status(500).json({ code: '99', message: error.message });
    }
};
