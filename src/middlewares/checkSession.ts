import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

interface JwtPayload {
    id: string;
}

export const checkSession = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.replace('Bearer ', '');

    if (!token) {
        return res.status(401).json({ code: "99", error: "Token de autenticación no proporcionado" });
    }

    jwt.verify(token, process.env.JWT_KEY as string, (err, user) => {
        if (err) {
            return res.status(403).json({ code: "99", error: "Token de autenticación inválido" });
        }
        next();
    });
};
