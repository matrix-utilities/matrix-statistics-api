
# Api en Node para generar las estadísticas de una matriz

Dado un array NxN enviado por post se retornará un objeto con las siguientes estadísticas:
- Valor máximo: El valor máximo encontrado en las matrices.
- Valor mínimo: El valor mínimo encontrado en las matrices.
- Promedio: El promedio de todos los valores de las matrices.
- Suma total: La suma total de todos los valores de las matrices.
- Matriz diagonal: Verificar si la matriz es diagonal.

## Ejecución:
1. Clonar repositorio
2. Instalar las depentencias utilizadas: "npm install"
3. Ejecutar con el comando que corresponda.

### Variables de entorno:

```
PORT=8080
HOST=0.0.0.0
JWT_KEY=test-123
```

### Para ejecutar en local:
```bash
npm run dev
```
La url de swagger en local: [http://127.0.0.1:8080/swagger/swagger.json](http://127.0.0.1:8080/swagger/swagger.json)

### Para ejecutar en prod:

```bash
npm run build
npm run start
```

## Despliegue con Docker:
1. cd /path/to/DockerfileFolder
2. docker build --pull --rm -f "Dockerfile" -t matrix-stats-api:1.0 "."
3. docker run -p 8080:8080 matrix-stats-api:1.0

## Despliegue con gcloud (Example):

```sh
gcloud.cmd builds submit --tag us-east4-docker.pkg.dev/project/matrix-utilities/project:dev .
```

## Author:
By [Orlando A. Yepes](https://www.linkedin.com/in/orlando-andr%C3%A9s-yepes-miquilena-9649544a/)
