import { Matrix, MatrixStatistics } from "../interfaces/matrix.interface";

export class MatrixService {
    static calculateStatistics(matrix: Matrix): MatrixStatistics {
        let max = -Infinity;
        let min = Infinity;
        let total = 0;
        let totalCount = 0;
        let isDiagonal = true;

        matrix.data.forEach((row, i) => {
            row.forEach((value, j) => {
                if (value > max) max = value;
                if (value < min) min = value;
                total += value;
                if (i !== j && value !== 0) isDiagonal = false;
            });
            totalCount += row.length;
        });

        return {
            max,
            min,
            average: total / totalCount,
            totalSum: total,
            isDiagonal
        };
    }
}
