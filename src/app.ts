import express from 'express';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import routes from './routes/routes';
import swaggerSpec from './config/swagger';

const app = express();
const PORT = process.env.PORT;
const HOST = process.env.HOST;
const DEFAULT_PORT = 3000;
let port: number;

if (process.env.PORT) {
    port = parseInt(process.env.PORT, 10);
    if (isNaN(port)) {
        console.error('Invalid port number. Using default port.');
        port = DEFAULT_PORT;
    }
} else {
    console.log('No port number specified. Using default port.');
    port = DEFAULT_PORT;
}

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(cors({
    origin: '*'
}));
app.use(express.json());
app.use(routes);

app.listen(port, '0.0.0.0', () => {
    console.log(`Server is running on ${HOST}:${PORT}`);
    console.log(`Swagger is running on ${HOST}:${PORT}/swagger/swagger.json`);
});

export default app;
