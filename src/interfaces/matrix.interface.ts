export interface Matrix {
    data: number[][];
}

export interface MatrixStatistics {
    max: number;
    min: number;
    average: number;
    totalSum: number;
    isDiagonal: boolean;
}
