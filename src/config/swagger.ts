import swaggerJSDoc from 'swagger-jsdoc';

const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || 'http://localhost';

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Matrix Statistics API',
            version: '1.0.0',
            description: 'This is a sample API for matrix operations.',
        },
        servers: [
            {
                url: `${HOST}:${PORT}`,
                description: 'Local server',
            },
        ],
    },
    // Rutas a los archivos con documentación JSDoc
    apis: ['./src/routes/*.ts', './src/controllers/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);


export default swaggerSpec;
