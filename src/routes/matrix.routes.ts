import { Router } from 'express';
import * as matrixController from '../controllers/matrix.controller';
import {checkSession} from "../middlewares/checkSession";

const router = Router();

router.post('/calculate', checkSession, matrixController.postMatrix);

export default router;
