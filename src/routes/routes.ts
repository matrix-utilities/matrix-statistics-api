import { Router } from 'express';
import matrixRoutes from './matrix.routes';

const router = Router();

router.get('/status', (req, res) => {
    res.status(200).send({ status: 'API is running' });
});

router.use('/stats/api/v1', matrixRoutes)

export default router;
